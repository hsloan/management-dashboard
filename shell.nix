with import <nixpkgs> {}; 

stdenv.mkDerivation rec {
  name = "management-dashboard-env"; 
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    nodejs
    python3
  ]; 
}
