# How to build, install, and run the web app as a DEB package?

In this simple guide, we will go through steps to deliver the web app as a package and run it on Linux.

# Build React App

If you have your build files from the react app, then you can skip this step

## Installing Dependencies

Just by running a simple command which is:

```bash
npm install
```

The _node_modules_ folder would be created

## Building the app

After installing the deps, run

```bash
npm run build
```

> Note: If you had a problem with mesh SDK, try to run this command
>
> ```bash
> npm i @meshsdk/core
> npm run build
> ```

# DEB Package Folder Structure

Create a folder with your package name like: **nunet-compute-request**

## Placing the Web app

go to **/nunet-compute-request/var/www/compute-request-webapp** By creating **var** folder inside the package then create **www** and Finally create **compute-request-webapp**
Then Copy everything in the React **build** folder to your **/nunet-compute-request/var/www/compute-request-webapp**
You should have it look like this:
![Picture of folder Structre](/docs/structure.png "Picture of folder Structre").

## Control Files

At this step we will add the package metadata and Scripts, you'll go to the root directory of the package again which is in our case is **/nunet-compute-request** and create a folder named **DEBIAN**
The folder structure of **/nunet-compute-request/DEBIAN** should have the following files:

- control
- postinst
- postrm
  -preinst
- prerem

![Picture of folder Structre](/docs/debian.png "Picture of folder Structre").

> Note: Don't add Extensions

### 1 - control

the metadata of the file

```
Package: nunet-compute-request-webapp
Version: 0.1.0
Architecture: amd64
Installed-Size: 35704
Depends: python3
Section: utils
Maintainer: Khaled Yasser
```

### 2 - postinst

A script that runs directly after installation to run the service, the code is explained in comments

```bash
#!/bin/bash

echo Running postinst script.

USER=nunet

# if there is no such user, create it
adduser --system --no-create-home --quiet $USER
addgroup --system $USER
adduser $USER $USER

# Give above user access to write to /etc/nunet
chown -R $USER:$USER /etc/nunet

# Copy Web app files
# Copy the app files to the web server directory
mkdir -p /var/www/myapp
cp -r var/www/compute-request-webapp/* /var/www/compute-request-webapp/

# Start the services.
systemctl daemon-reload
systemctl enable --now nunet-compute-request.service  # enable --now == enable and start

```

### 3 - postrm

A script that runs directly after removing of package

```bash
#!/bin/bash

systemctl daemon-reload

# don't need to remove the group because the group is deleted along with the user
# given that group only has that particular user
if [ -x "$(command -v deluser)" ]; then
     deluser --quiet --system nunet > /dev/null || true
  else
     echo >&2 "not removing nunet system account because deluser command was not found"
fi
```

### 4 - preinst

A script that runs directly before installing the package

```bash
#!/bin/bash

echo "Running pre installation script ..."

# Pre-install script for nunet-compute-request.

# Part 1: Remove old version of nunet-compute-request.
# 0. Stop running service. Remove the service unit files.
# 1. Remove binary file.
# 2. Remove config files at /etc/nunet

echo "Stopping service and removing service unit files ..."

systemctl daemon-reload

if [ -f "/etc/systemd/system/nunet-compute-request.service" ];then
    systemctl stop nunet-compute-request.service
    rm /etc/systemd/system/nunet-compute-request.service
fi

echo "Looking for old versions of nunet-compute-request ..."

if [ -f "/usr/bin/nunet-compute-request" ];then
    rm -f /usr/bin/nunet-compute-request
    echo "Removed old nunet-compute-request from /usr/bin ..."
fi

```

### 5 - prerm

A script that runs directly before removing the package

```bash
#!/bin/bash

# Stop the service
systemctl stop nunet-compute-request.service
```

## The Service file

Go to the root directory and create **/etc** and inside it **systemd** folder and inside it create **system** folder, so the Structure should be: <br/>
**/nunet-compute-request/etc/systemd/system**
create a file named

> nunet-compute-request.service
> That will be the background service that will toss our server

```yaml
[Unit]
Description=NuNet Compute Request Webapp
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=nunet

Environment=GIN_MODE=release
ExecStart=python3 -m http.server 9991 --bind 127.0.0.1 --directory /var/www/compute-request-webapp/

Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

> Knowing That we use python HttpServer model to run our server, **You can identify the address and the port from this file**

# Packaging

## create the deb file

You'll go outside the root directory, open the terminal then run:

```bash
dpkg-deb --build nunet-compute-request
```

That Should create a deb package with the same name

## Installing

run The following Command on terminal

```bash
sudo dpkg -i nunet-compute-request.deb
```

That will install your package, Just visit the link you defined in **nunet-compute-request.service** to use the web app. in our case it will be

> 127.0.0.1:9991
