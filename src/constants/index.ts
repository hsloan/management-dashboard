export const TENSORFLOW_REGISTRY =
  "registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/tensorflow";

export const PYTORCH_REGISTRY =
  "registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/pytorch";

export const SERVICE_TYPE = "ml-training-gpu";

export const SCRIPT_ADDRESS =
  "addr_test1wplx9dwzmn986k48kwmqn75yjlhlwcy094euq8c7s2ws8xc5k5uu6";

export const BACKEND_ENDPOINT = "http://127.0.0.1:9999/api/v1/run/claim";
