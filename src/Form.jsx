import React from "react";
import { NunetParticles } from "./components/Particles";
import {
  Container,
  Header,
  Content,
  Form,
  ButtonToolbar,
  Button,
  Navbar,
  Panel,
  FlexboxGrid,
  Radio,
  RadioGroup,
  Whisper,
  Tooltip,
  IconButton,
  Nav,
} from "rsuite";
import SendIcon from "@rsuite/icons/Send";
import InfoRoundIcon from "@rsuite/icons/InfoRound";
import { getClaim } from "./services/api";
import { useLocalStorage } from "./hooks/useLocalStorage";
import Logo from "./logo.png";

import { Alert } from "./Alert/Alert";

import { useWallet, useAddress, CardanoWallet } from "@meshsdk/react";

import { IS_PRODUCTION } from "./settings";

export const AppForm = () => {
  const { connected } = useWallet();

  const [response, setResponse] = useLocalStorage("response", {});

  const address = useAddress();

  const [form, setForm] = React.useState({
    blockChain: "Cardano",
  });

  const [errors, setErrors] = React.useState([]);

  const onSubmit = () => {
    setErrors([]);

    const collectedErrors = [];

    if (collectedErrors.length > 0 && IS_PRODUCTION) {
      setErrors([...collectedErrors]);
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    } else {
      getClaim(address).then((x) => setResponse(x));
    }
  };

  return (
    // <NunetLoader duration={2000} onFinsih={() => {}}>
    <div className="show-fake-browser login-page">
      <Container>
        <Header style={{ zIndex: "99999" }}>
          <Navbar appearance="inverse" style={{ background: "#0a2042b9" }}>
            <Navbar.Brand style={{ textAlign: "left" }}>
              <span style={{ color: "#fff" }}>
                <img
                  src={Logo}
                  alt="logo"
                  style={{
                    width: "100px",
                    marginTop: "-5px",
                    textAlign: "left",
                  }}
                />
              </span>
            </Navbar.Brand>
            <Nav pullRight className="CardanoWallet">
              <CardanoWallet />
            </Nav>
          </Navbar>
        </Header>
        {errors.length > 0 && <Alert text={errors[0]} />}
        {/* HERE! */}
        <NunetParticles />
        <Content>
          <FlexboxGrid justify="center" style={{ margin: "2rem 0" }}>
            <FlexboxGrid.Item colspan={12} className="flex-panel-container">
              <Panel
                style={{ background: "white", padding: "10px" }}
                className="flex-panel"
                header={
                  <h4
                    style={{
                      textAlign: "center",
                      fontWeight: "300",
                      marginBottom: "15px",
                      position: "relative",
                      fontSize: "30px",
                    }}
                  >
                    Request NTX Tokens 🪙
                    <hr />
                  </h4>
                }
                bordered
                shaded
              >
                <Form fluid onSubmit={onSubmit}>
                  <Form.Group name="blockchain" label="Blockchain">
                    <Form.ControlLabel style={{ position: "relative" }}>
                      Blockchain:
                      <Whisper
                        trigger="hover"
                        placement="left"
                        speaker={
                          <Tooltip>
                            The blockchain needed, Etherum is still in
                            development
                          </Tooltip>
                        }
                      >
                        <IconButton
                          size="xs"
                          circle
                          style={{
                            position: "absolute",
                            right: "0",
                            top: "-3px",
                          }}
                          icon={<InfoRoundIcon color="#78909b" />}
                        />
                      </Whisper>
                    </Form.ControlLabel>
                    <RadioGroup
                      name="blockchain"
                      color="#0A2042"
                      inline
                      value={form.blockChain}
                      onChange={(v) => setForm({ ...form, blockChain: v })}
                    >
                      <Radio value={"Cardano"}>Cardano</Radio>
                      <Radio disabled={true} value={"Ethereum"}>
                        Ethereum (In Development)
                      </Radio>
                    </RadioGroup>
                  </Form.Group>
                  <Form.Group>
                    <ButtonToolbar>
                      <Button
                        appearance="primary"
                        size="lg"
                        className="gr-btn"
                        style={{ width: "100%" }}
                        type="submit"
                        disabled={!connected && IS_PRODUCTION}
                      >
                        <SendIcon /> Request
                      </Button>
                      {!connected && IS_PRODUCTION && (
                        <Form.HelpText>
                          You have to connect a wallet to submit.
                        </Form.HelpText>
                      )}
                    </ButtonToolbar>
                  </Form.Group>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
      </Container>
    </div>
    // </NunetLoader>
  );
};
