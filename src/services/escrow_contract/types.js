const giveParams = {
  gpBeneficiary: "",
  gpOracle: "",
  gpUserAddress: "",
  gpDeadline: 0,
  gpRedeemActionToken: "",
  gpAmount: 0,
  gptokenPolicyId: "",
  gptokenName: "",
  gpNTXTokenAmount: "",
};

const grabParams = {
  gbtokenPolicyId: "",
  gbtokenName: "",
  gbOracle: "",
  gbUserAddress: "",
  gbDeadline: 0,
  gbRedeemActionToken: "",
};
